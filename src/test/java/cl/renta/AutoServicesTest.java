package cl.renta;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.renta.dao.AutoDao;
import cl.renta.model.Auto;
import cl.renta.service.AutoService;

@RunWith(MockitoJUnitRunner.class)
public class AutoServicesTest {

	@Mock
	private AutoDao autodao;
	
	@InjectMocks
	private AutoService autoservice;
	
	@Test
	public void listarTodosLosAutosPorCategoria(){
		//arrange
		List<Auto> misAutos = new ArrayList<Auto>();
		List<Auto> resultado = new ArrayList<Auto>();
		
		//act
		when(autodao.findByCategoria(anyString())).thenReturn(misAutos);
		resultado = autoservice.obtenerTodosLosAutosPorCategoria("Camioneta");
		
		//assert
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultado, misAutos);
	}
	
	

}
