package cl.renta;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import static org.mockito.Matchers.anyLong;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import cl.renta.dao.ClienteDao;
import cl.renta.model.Cliente;
import cl.renta.service.ClienteService;

@RunWith(MockitoJUnitRunner.class)
public class RentaAutosServicesTest {

	@Mock
	private ClienteDao clienteDao;
	
	@InjectMocks
	private ClienteService clienteService;
	
	@Test
	public void registrarUnCliente(){
		//arrange
		Cliente cliente=new Cliente();
		
		//act
		when(clienteDao.save(cliente)).thenReturn(cliente);
		Cliente clienteCreado = clienteService.registrarCliente(cliente);
		
		//assert 
		Assert.assertNotNull(clienteCreado);
		Assert.assertEquals(cliente, clienteCreado);
	}
	
	@Test
	public void listarTodosLosClientes(){
		//arrange 
		ArrayList<Cliente> listaClientes=new ArrayList<Cliente>();
		ArrayList<Cliente> resultado=new ArrayList<Cliente>();
		
		//act 
		when(clienteDao.findAll()).thenReturn(listaClientes);
		resultado = clienteService.obtenerTodosLosClientes();
		
		//assert 
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultado, listaClientes);
	}
	
	@Test
	public void consultaSiClienteEstaRegistrado(){
		//arrange
		Cliente cliente=new Cliente();
		boolean resultadoEsperado=true;
		boolean resultado;
		
		//act 
		when(clienteDao.findByRut(cliente.getRut())).thenReturn(resultadoEsperado);
		resultado=clienteService.buscarCliente(cliente.getRut());

		//assert
		Assert.assertTrue(resultado);
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultadoEsperado, resultado);
	}
}
