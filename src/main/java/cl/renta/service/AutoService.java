package cl.renta.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.renta.dao.AutoDao;
import cl.renta.model.Auto;

@Service
public class AutoService {
	
	@Autowired
	private AutoDao autodao;
	
	public List<Auto> obtenerTodosLosAutosPorCategoria(String categoria) {
		// TODO Auto-generated method stub
		List<Auto> misAutos = new ArrayList<Auto>();
		misAutos = (List<Auto>) autodao.findByCategoria(categoria); 
		return misAutos;
	}

	


}
