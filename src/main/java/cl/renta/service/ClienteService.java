package cl.renta.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import cl.renta.dao.ClienteDao;
import cl.renta.model.Cliente;
import cl.renta.service.ClienteService;

public class ClienteService {

	@Autowired
	private static ClienteDao clienteDao;
	
	@Autowired
	public ClienteService(ClienteDao clienteDao){
		ClienteService.clienteDao=clienteDao;
	}

	public Cliente registrarCliente(Cliente cliente) {
		
		return clienteDao.save(cliente);
	}

	public ArrayList<Cliente> obtenerTodosLosClientes() {

		ArrayList<Cliente> listaClientes = new ArrayList<Cliente>();
		return listaClientes;
	}

	public boolean buscarCliente(String rut) {
				
		return clienteDao.findByRut(rut);
	}

	
}
